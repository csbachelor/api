from flask import Flask, jsonify, request, send_file, render_template, Response
import requests
from skimage import io, util
from io import BytesIO
from base64 import b64encode
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.distributions as D
from dotmap import DotMap
from PIL import Image, ImageDraw
import time

app = Flask(__name__)

class Net(nn.Module):
    
    def __init__(self,config):
        super().__init__()

        conv_layers = [3]+[config.channels_1]*2
        conv_kernelsize = [3,3]
        conv_stride = [1,1]
        conv_padding = [1,1]
        conv_activation = [nn.ReLU,nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_1]*2

        
        self.conv1 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout)

        conv_layers = [config.channels_1]+[config.channels_2]*2 
        conv_kernelsize = [3,3]
        conv_stride = [1,1]
        conv_padding = [1,1]
        conv_activation = [nn.ReLU,nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_2]*2
        self.conv2 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout)
        conv_layers = [config.channels_2]+[config.channels_3]*3 
        conv_kernelsize = [3,3,3]
        conv_stride = [1,1,1]
        conv_padding = [1,1,1]
        conv_activation = [nn.ReLU,nn.ReLU, nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_3]*3
        self.conv3 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout)  
        conv_layers = [config.channels_3]+[config.channels_4]*3 
        conv_kernelsize = [3,3,3]
        conv_stride = [1,1,1]
        conv_padding = [1,1,1]
        conv_activation = [nn.ReLU,nn.ReLU, nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_4]*3
        self.conv4 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout) 
        conv_layers = [config.channels_4]+[config.channels_5]*3 
        conv_kernelsize = [3,3,3]
        conv_stride = [1,1,1]
        conv_padding = [1,1,1]
        conv_activation = [nn.ReLU,nn.ReLU, nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_5]*3
        self.conv5 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout)                             
        
        self.features_cnn = conv_layers[-1] * 1*1
        
        # fully connected output layers
        
        fc_layers = [self.features_cnn]+[config.channels_d]*2 #[self.features_cat_size, 4096, 4096, 1024, self.num_classes]
        fc_activations = [nn.ReLU, nn.ReLU] #[nn.ReLU, nn.ReLU, nn.ReLU, nn.Identity]
        fc_biases = [True,True] #[True, True, False, False]
        fc_normfuncs = [nn.BatchNorm1d,nn.BatchNorm1d]
        fc_dropouts = [config.dropout_d]*2 #[0.4, 0.3] #[0.5, 0.5, 0.5, 0.5]
        
        self.fc = self.create_fully_connected_layer(layers=fc_layers, activation_functions=fc_activations, 
                                                    bias_bools=fc_biases, normfuncs=fc_normfuncs, dropouts=fc_dropouts)
        self.fc_out = nn.Linear(fc_layers[-1],1)

    @staticmethod
    def create_fully_connected_layer(layers, activation_functions, bias_bools, normfuncs, dropouts):
        fc = nn.Sequential()
        for idx, _ in enumerate(layers):
            if idx == 0:
                continue
            # Extract the in and out channel numbers and layer specifications
            in_channel = layers[idx-1]
            out_channel = layers[idx]
            activation_function = activation_functions[idx-1]
            bias_bool = bias_bools[idx-1]
            normfunc = normfuncs[idx-1]
            dropout = dropouts[idx-1]
            
            fc.add_module('fc{}'.format(idx),
                nn.Sequential(
                    nn.Dropout(dropout), # dropout (before linear function as in Srivastava et al, 2014)
                    nn.Linear(in_channel, out_channel, bias=bias_bool),
                    normfunc(out_channel), # batch normalization before activation function as suggested in Ioffe and Szegedy 2015
                    activation_function(inplace=True)
                )
            )
        
        return fc
    
    
    @staticmethod
    def create_convolutional_layer(layers, kernel_sizes, strides, paddings, activation_functions, pool_kernel_size, pool_stride, pool_padding, dropouts):
        conv = nn.Sequential()
        
        for idx, _ in enumerate(layers):
            if idx == 0:
                continue
            # Extract the in and out channel numbers and layer specifications
            in_channels = layers[idx-1]
            out_channels = layers[idx]
            kernel_size = kernel_sizes[idx-1]
            stride = strides[idx-1]
            padding = paddings[idx-1]
            activation_function = activation_functions[idx-1]
            dropout = dropouts[idx-1]
            
            conv.add_module('conv{}'.format(idx),
                nn.Sequential(
                    nn.Dropout(dropout),
                    nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
                    nn.BatchNorm2d(out_channels), # batch normalization before activation function as suggested in Ioffe and Szegedy 2015
                    activation_function(inplace=True)
                )
            )
        
        conv.add_module('maxpool',nn.MaxPool2d(kernel_size=pool_kernel_size, stride=pool_stride, padding=pool_padding))
        return conv
    
    def forward(self, x):
        # Your code here!
        
        ## transform the input
        #x = self.stn(x)
        ## save transformation
        #l_trans1 = Variable(x.data)

        # Perform the usual forward pass
        
        # Convolutional network
        x = x.permute(0,3,1,2)
        x = self.conv1(x.float())
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        x = x.view(-1,self.features_cnn)
        x = self.fc(x)
        # return output and batch of bilinear interpolated images
        return self.fc_out(x)

hyperparameter_defaults = dict(
    dropout_1 = 0.21,
    dropout_2 = 0.7093,
    dropout_3 = 0.4972,
    dropout_4 = 0.722,
    dropout_5 = 0.5342,
    dropout_d = 0.8966,
    channels_1 = 64,
    channels_2 = 128,
    channels_3 = 256,
    channels_4 = 512,
    channels_5 = 512,
    channels_d = 4096,
    batch_size = 32,
    learning_rate = 0.06081,
    epochs = 128,
    )
config = DotMap(hyperparameter_defaults)
model = Net(config).to("cpu")
state = torch.load("model_final.pt", map_location="cpu")
model.load_state_dict(state)
model.eval()
@app.route('/probabilities/', methods=['GET'])
def api():
    mapname = request.args.get('mapname', type=str)
    x = request.args.get('x', type=float)
    y = request.args.get('y', type=float)
    z = request.args.get('z', type=float)
    pitch = request.args.get('pitch', type=float)
    yaw = request.args.get('yaw', type=float)
    absolute = request.args.get('absolute', default = False, type=bool)
    format = request.args.get('format',default='json',type=str)

    if not (mapname and x and y and z and pitch and yaw):
        return 'Location parameters are not included in request', 400

    if format.lower() == 'image':

        if absolute:
            link = (
                        f'https://screenshoter.scope.services/'
                        f'{mapname}?'
                        f'x={x}&y={y}&z={z}'
                        f'&pitch={pitch}&yaw={yaw}'
                    )
        else:
            
            z=float(z)+64
            link = (
                        f'https://screenshoter.scope.services/'
                        f'{mapname}?'
                        f'x={x}&y={y}&z={z}'
                        f'&pitch={pitch}&yaw={yaw}'
                    )
        
        r = requests.get(link)
        try:
            im = io.imread(BytesIO(r.content))
        except:
            return 'Image couldnt be generated by scope.gg',400
        image = Image.fromarray(im)
        H,W,C = im.shape
        draw = ImageDraw.Draw(image)
        draw.rectangle(((W-48)//2, (H-48)//2, (W+48)//2, (H+48)//2), outline=(0, 0, 256))
        byte_io = BytesIO()
        image.save(byte_io,format='PNG')
        image = byte_io.getvalue()
        byte_io.close()
        im = util.crop(im,(((H-48)/2,(H-48)/2),((W-48)/2,(W-48)/2),(0,0)))
        im = torch.from_numpy(im)
        im = im.unsqueeze(0)
        try:
            preds = model(im).view(-1).float()
        except:
            return 'Model couldnt parse image',400



        return render_template("index.html",prob=torch.sigmoid(preds).item(), picture=b64encode(image).decode("utf-8"))
    elif format.lower() == 'json':    
        if absolute:
            link = (
                        f'https://screenshoter.scope.services/'
                        f'{mapname}?'
                        f'x={x}&y={y}&z={z}'
                        f'&pitch={pitch}&yaw={yaw}'
                    )
        else:
            z=float(z)+64
            link = (
                        f'https://screenshoter.scope.services/'
                        f'{mapname}?'
                        f'x={x}&y={y}&z={z}'
                        f'&pitch={pitch}&yaw={yaw}'
                    )
        
        t1 = time.time()
        r = requests.get(link)
        t2 = time.time()
        try:
            im = io.imread(BytesIO(r.content))
        except:
            return 'Image couldnt be generated by scope.gg',400
        H,W,C = im.shape
        im = util.crop(im,(((H-48)/2,(H-48)/2),((W-48)/2,(W-48)/2),(0,0)))
        im = torch.from_numpy(im)
        im = im.unsqueeze(0)
        t3 = time.time()
        try:
            preds = model(im).view(-1).float()
        except:
            return 'Model couldnt parse image',400
        t4 = time.time()
        
        return jsonify({'probability':torch.sigmoid(preds).item()})#, 'request_time':t2-t1, 'preprocess_time':t3-t2,'feedforward':t4-t3})
    else:
        return 'Format parameter is not image nor json', 400





if __name__ == '__main__':
    
    app.run(debug=True)